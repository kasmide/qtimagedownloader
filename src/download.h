#ifndef DOWNLOAD_H_INCLUDED
#define DOWNLOAD_H_INCLUDED

#include <QUrl>
#include <QWidget>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QObject>
#include <QMessageBox>
#include <QEventLoop>
#include <QFile>
#include <QProgressDialog>
#include <QtConcurrent>
#include <QThread>
#include <QDir>
#include <KLocalizedString>

class Downloader : public QObject{
Q_OBJECT

private:
    QUrl pageUrl;
    QString destDir;
	bool serialFileName=false;
    int progressValue=0;
	QString source;
	QVector<QString> imgslist;
	bool aElements;
	bool imgElements;
	void download(QUrl url,QString fileName){
		QNetworkAccessManager *manager = new QNetworkAccessManager();
		QEventLoop eventLoop;
		QObject::connect(manager, &QNetworkAccessManager::finished,&eventLoop, &QEventLoop::quit);
		QNetworkReply *reply = manager->get(QNetworkRequest(url));
		eventLoop.exec();
		if(reply->error() == QNetworkReply::NoError){
			//リダイレクト時の処理 (リダイレクトされたら要求URLを変更して再試行)
			QUrl redirectUrl = reply->attribute(QNetworkRequest::RedirectionTargetAttribute).toUrl();
			if(!redirectUrl.isEmpty())
			{
                //再ダウンロード
				download(redirectUrl.toString(),fileName);
			}
			else
			{
				QFile file;
				file.setFileName(fileName);
				file.open(QFile::ReadWrite);
				file.write(reply->readAll());
				file.close();
                progressValue++;
				emit finished();
			}
		}else{
            progressValue++;
			emit finished();
            QMessageBox errBox;
            errBox.setText(i18n("Failed to download Image file: %1",url.toString()));
            errBox.exec();
		}
		delete manager;
	}

signals:
	void finished();
public:
    Downloader(QUrl url,QString dest){pageUrl=url;destDir=dest;}
	void setFileNameSerials(bool i){
		serialFileName=i;
	}
	void setDownloadTag(bool a,bool img){
		aElements=a;
		imgElements=img;
	}
    
    void start(){
        QMessageBox qbox;
        QNetworkAccessManager *manager = new QNetworkAccessManager(this);
		QEventLoop eventLoop;
        QObject::connect(manager, &QNetworkAccessManager::finished,&eventLoop, &QEventLoop::quit);
		QNetworkReply *reply = manager->get(QNetworkRequest(pageUrl));
		eventLoop.exec();
		if(reply->error() == QNetworkReply::NoError){
            source=QString::fromLocal8Bit(reply->readAll());
		}else{
            qbox.setText(i18n("Couldn't download HTML file"));
            qbox.exec();
            return;
        }
		delete manager;
        getimgslist();
        if(imgslist.size()==0){
            qbox.setText(i18n("No files found"));
			qbox.exec();
			return;
		}
		QDir dir;
		dir.mkdir(destDir);
		for(int i=0;i<imgslist.size();i++){
			QString fileName;
			if(serialFileName==true){
				fileName=destDir+"/"+QString::number(i+1).rightJustified(QString::number(imgslist.size()).length(),'0')+imgslist[i].mid(imgslist[i].lastIndexOf("."));
			}
			else
			{
				fileName=destDir+"/"+imgslist[i].mid(imgslist[i].lastIndexOf("/"));
			}
			QtConcurrent::run(this,&Downloader::download,QUrl(imgslist[i]),fileName);
		}
		QProgressDialog progress(i18n("Downloading images..."), i18n("Close"), 0, imgslist.size());
        progress.setMinimumDuration(0);
        progress.setValue(0);
        while(progressValue<imgslist.size()){
            QEventLoop loop;
			connect(this,SIGNAL(finished()),&loop,SLOT(quit()));
			loop.exec();
            progress.setValue(progressValue);
            }
	}

    void getimgslist(){
		int tagPosition=0;
		if(aElements){
            while(source.indexOf("href=",source.indexOf("<a",tagPosition))!=-1){
                tagPosition=source.indexOf("href=",source.indexOf("<a",tagPosition))+5;
                sieve_and_set(getFileName(tagPosition));
            }
        }
		tagPosition=0;
		if(imgElements){
            while(source.indexOf("src=",source.indexOf("<img",tagPosition))!=-1){
				tagPosition=source.indexOf("src=",source.indexOf("<img",tagPosition))+4;
                sieve_and_set(getFileName(tagPosition));
            }
		}
    }
    QString getFileName(int tagPosition){
        //FIXME: だれかこのクソコードをなんとかしてください。
        int lastPosition = INT_MAX;
        if(source.indexOf(" ",tagPosition)!=-1)
            lastPosition=source.indexOf(" ",tagPosition);

        if(source.indexOf(">",tagPosition)<=lastPosition&&source.indexOf(">",tagPosition)!=-1)
            lastPosition=source.indexOf(">",tagPosition);

        if(source.indexOf("/>",tagPosition)<=lastPosition&&source.indexOf("/>",tagPosition)!=-1)
            lastPosition=source.indexOf("/>",tagPosition);
        
        if(lastPosition!=INT_MAX){
            QString fileName=source.mid(tagPosition,lastPosition-tagPosition);
            
            if(fileName.mid(0,1)=="\"" || fileName.mid(0,1)=="'")
            {
                fileName=fileName.mid(1,fileName.size()-2);
            }
            return fileName;
            
        }else{
            //QDebug("ファイル名の取得で問題が発生しました: %1",pageUrl);
            return "";
        }
    }
    void sieve_and_set(QString fileName){
		QString fileExtension=fileName.mid(fileName.lastIndexOf("."));
		if(fileExtension==".jpg" || fileExtension==".png" || fileExtension==".gif" || fileExtension==".svg")
		{
			if(fileName.mid(0,1)=="/"){
                //相対パス(eg. src="/hello/hello.jpg")時にフルパスを取得
                fileName=pageUrl.toString().mid(0,pageUrl.toString().indexOf("/",pageUrl.toString().indexOf("://")+3)+1)+fileName;
			}
			else if(fileName.indexOf("://")==-1){
                //相対パス(eg. src="hello/hello.png")時にフルパスを取得
				fileName=pageUrl.toString().mid(0,pageUrl.toString().lastIndexOf("/")+1)+fileName;
			}
			imgslist.insert(imgslist.size(),fileName);
		}
	}
};

#endif // DOWNLOAD_H_INCLUDED
