#include "widget.h"
#include <QApplication>
#include <QIcon>
#include <KLocalizedString>

int main(int argc, char *argv[])
{
    KLocalizedString::setApplicationDomain("qtimagedownloader");
    QGuiApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    QApplication a(argc, argv);
    Widget w;
    w.setWindowFlags(Qt::WindowStaysOnTopHint);
    w.show();
    return a.exec();
}
