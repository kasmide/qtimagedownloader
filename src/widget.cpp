#include "download.h"
#include "widget.h"
#include "ui_widget.h"

#include <QFileDialog>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
	ui->lineEdit->setFocus();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_startButton_clicked()
{
	ui->startButton->setEnabled(false);
    QString downloadURI=ui->lineEdit->text();
    if(downloadURI.indexOf("/",ui->lineEdit->text().indexOf("://")+3)==-1)downloadURI=downloadURI+"/";
    Downloader download(downloadURI,ui->dirEdit->text());
    download.setFileNameSerials(ui->serialFileName->isChecked());
    download.setDownloadTag(ui->radioButton_a->isChecked(),ui->radioButton_img->isChecked());
    download.start();
	ui->startButton->setEnabled(true);
}

void Widget::on_toolButton_clicked()
{
    QFileDialog folder;
    ui->dirEdit->setText(folder.getExistingDirectory().toUtf8());
}
